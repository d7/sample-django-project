from concurrent import futures
from datetime import datetime

import pytest

from apps.base.utils import get_numeric_id


class TestGetNumericID:

    def test_uses_microsecond_part(self, mocker):
        mocker.patch('apps.base.utils.time.time', return_value=1234.567890)
        number = get_numeric_id()
        assert str(number)[:7] == '1234567'

    @pytest.mark.parametrize('part', [1, 12])
    def test_uses_zero_padded_machine_id_part(self, mocker, part):
        mocker.patch('apps.base.utils.settings.MACHINE_ID', new=part)
        number = get_numeric_id()
        assert str(number)[-4:-2] == str(part).zfill(2)

    @pytest.mark.parametrize('part', [1, 12])
    def test_uses_zero_padded_random_part(self, mocker, part):
        mocker.patch('apps.base.utils.random.randint', return_value=part)
        number = get_numeric_id()
        assert str(number)[-2:] == str(part).zfill(2)

    def test_number_is_unique(self):
        promises = []
        with futures.ThreadPoolExecutor(10) as pool:
            for i in range(1000):  # Generate numbers sharing the same ms async
                future = pool.submit(get_numeric_id)
                promises.append(future)
        numbers = [f.result() for f in promises]
        assert len(numbers) == len(set(numbers))  # Uniqueness

    @pytest.mark.parametrize('year', [2020, 2025, 2050, 2100, 2200])
    def test_number_fits_within_bigint_8_bytes(self, mocker, year):
        time = datetime(year, 1, 1).timestamp()
        mocker.patch('apps.base.utils.time.time', return_value=time)
        number = get_numeric_id()
        assert number < 2 ** 63 - 1  # 63 bits == 8 bytes - sign bit

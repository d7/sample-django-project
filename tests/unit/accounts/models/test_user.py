import pytest

from apps.accounts.models import User, UserManager


class TestObjects:

    def test_uses_our_manager(self):
        assert isinstance(User.objects, UserManager)


class TestStr:

    def test_formats_with_user_uuid(self):
        user = User(uuid='6220cfce')
        assert str(user) == "User 6220cfce"


class TestGetFullName:

    @pytest.mark.parametrize('user', [
        User(name="John Doe"),
        User(name="John Doe", email="doe@foo.bar"),
        User(name="John Doe", email="doe@foo.bar", uuid='6220cfce'),
    ])
    def test_gets_name_with_precedence(self, user):
        assert user.get_full_name() == "John Doe"

    @pytest.mark.parametrize('user', [
        User(email="doe@foo.bar"),
        User(email="doe@foo.bar", uuid='6220cfce'),
    ])
    def test_gets_email_if_name_undefined(self, user):
        assert user.get_full_name() == "doe@foo.bar"

    def test_falls_back_to_str_if_needed(self):
        user = User()
        assert user.get_full_name() == str(user)


class TestGetShortName:

    @pytest.mark.parametrize('user', [
        User(name="John"),
        User(name="John Doe"),
        User(name="John Doe", email="doe@foo.bar"),
        User(name="John Doe", email="doe@foo.bar", uuid='6220cfce'),
    ])
    def test_gets_first_name_with_precedence(self, user):
        assert user.get_short_name() == "John"

    @pytest.mark.parametrize('user', [
        User(email="doe@foo.bar"),
        User(email="doe@foo.bar", uuid='6220cfce'),
    ])
    def test_gets_email_username_if_name_undefined(self, user):
        assert user.get_short_name() == "doe"

    def test_falls_back_to_str_if_needed(self):
        user = User()
        assert user.get_short_name() == str(user)

import pytest

from apps.accounts.models import UserManager


class TestCreateUser:

    @pytest.mark.parametrize('extra_fields', [
        {},
        {'is_staff': True},
        {'is_superuser': True},
    ])
    def test_disables_superpowers(self, mocker, extra_fields):
        manager = UserManager()
        model = mocker.patch.object(manager, 'model')
        manager.create_user("doe@foo.bar", "John", "123", **extra_fields)
        assert model.call_args.kwargs['is_staff'] is False
        assert model.call_args.kwargs['is_superuser'] is False


class TestCreateSuperuser:

    @pytest.mark.parametrize('extra_fields', [
        {},
        {'is_staff': False},
        {'is_superuser': False},
    ])
    def test_enables_superpowers(self, mocker, extra_fields):
        manager = UserManager()
        model = mocker.patch.object(manager, 'model')
        manager.create_superuser("doe@foo.bar", "John", "123", **extra_fields)
        assert model.call_args.kwargs['is_staff'] is True
        assert model.call_args.kwargs['is_superuser'] is True

import subprocess


def test_python_code_complies_with_pep8(base_dir):
    """
    Run Flake8 checks in our code base
    """
    flake8 = subprocess.run('flake8', cwd=base_dir, capture_output=True)
    assert flake8.returncode == 0, flake8.stdout.decode()

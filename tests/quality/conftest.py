import pathlib

import pytest


@pytest.fixture
def base_dir():
    """
    The base project directory
    """
    project_dir = pathlib.Path(__file__).parents[2]  # Up two levels
    return str(project_dir)

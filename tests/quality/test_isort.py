import shlex
import subprocess


def test_python_imports_are_sorted_correctly(base_dir):
    """
    Run isort to check for bad Python imports sorting
    """
    cmd = shlex.split('isort --check-only --recursive --diff .')
    isort = subprocess.run(cmd, cwd=base_dir, capture_output=True)
    assert isort.returncode == 0, isort.stdout.decode()

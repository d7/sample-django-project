FROM python:3.8

# Set global env vars
ENV PYTHONUNBUFFERED=1
ENV PYTHONDONTWRITEBYTECODE=1

# Install the PostgreSQL client
RUN apt-get update && apt-get install -y postgresql-client

# Install Python packages
COPY misc/python/requirements.txt /tmp/requirements.txt
RUN pip install -qU pip && pip install -r /tmp/requirements.txt

# Set a working directory
WORKDIR /app

# Install the app
COPY . .

# Build static files
RUN SECRET_KEY=none ./manage.py collectstatic --noinput

# Run the app
CMD uwsgi --ini config/uwsgi.ini --http=0.0.0.0:${PORT:-8000}

from django.contrib.auth import models as auth
from django.db import models
from django.utils.translation import gettext as _

from apps.base.models import BaseModel


class UserManager(auth.BaseUserManager):
    """
    Implement the Django ORM manager for the User model
    """

    def _create_user(self, email, name, password, **extra_fields):
        email = self.normalize_email(email)
        user = self.model(email=email, name=name, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, name, password=None, **extra_fields):
        extra_fields.update(is_staff=False, is_superuser=False)
        return self._create_user(email, name, password, **extra_fields)

    def create_superuser(self, email, name, password, **extra_fields):
        extra_fields.update(is_staff=True, is_superuser=True)
        return self._create_user(email, name, password, **extra_fields)


class User(BaseModel, auth.AbstractBaseUser, auth.PermissionsMixin):
    """
    The main user account
    """

    email = models.EmailField(
        help_text=_("The email address of this user."),
        verbose_name=_("email address"),
        unique=True,
    )

    name = models.CharField(
        help_text=_("The name of this user."),
        max_length=120,
        verbose_name=_("name"),
    )

    is_active = auth.User._meta.get_field("is_active")

    is_staff = auth.User._meta.get_field("is_staff")

    objects = UserManager()

    USERNAME_FIELD = 'email'
    EMAIL_FIELD = 'email'
    REQUIRED_FIELDS = ['name']

    class Meta:
        verbose_name = _("user")
        verbose_name_plural = _("users")

    def __str__(self):
        return f"User {self.uuid}"

    def clean(self):
        super().clean()
        self.email = self.__class__.objects.normalize_email(self.email)

    def get_full_name(self):
        """
        Get the full name or email address of the user
        """
        return self.name or self.email or str(self)

    def get_short_name(self):
        """
        Get the short name or email address username of the user
        """
        if self.name:
            return self.name.split(maxsplit=1)[0]
        if self.email:
            return self.email.split('@', maxsplit=1)[0]
        return str(self)

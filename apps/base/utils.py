import random
import time

from django.conf import settings


def get_numeric_id() -> int:
    """
    Generate a unique ID number that fits within 8 bytes

    It's based on:
    - Current time (microsecond level)
    - The machine ID (for multi-node support)
    - A random number (for multi-process support)
    """
    now = int(time.time() * 100_000)
    machine_id = settings.MACHINE_ID
    rnd = random.randint(0, 99)
    number = int(f'{now}{machine_id:02d}{rnd:02d}')
    return number

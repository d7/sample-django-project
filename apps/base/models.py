from uuid import uuid4

from django.db import models
from django.utils.translation import gettext as _


class BaseModel(models.Model):
    """
    Base fields and methods for every model in this project
    """

    uuid = models.UUIDField(
        default=uuid4,
        editable=False,
        help_text=_('The UUID of this object.'),
        primary_key=True,
        unique=True,
        verbose_name=_('UUID'),
    )

    created = models.DateTimeField(
        auto_now_add=True,
        help_text=_('The date this object was created.'),
        verbose_name=_('created'),
    )

    last_updated = models.DateTimeField(
        auto_now=True,
        help_text=_('The date this object was last updated.'),
        verbose_name=_('last updated'),
    )

    class Meta:
        abstract = True
